resource "aws_instance" "dev" {
  ami                  = "ami-0915bcb5fa77e4892"
  instance_type        = "t2.micro"
  subnet_id            = "subnet-2b221e61"
  iam_instance_profile = "ssm-role"
  #key_name             = "test"
  vpc_security_group_ids      = ["sg-0fc94f4fafeca8da7"]
  }